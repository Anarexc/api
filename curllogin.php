<?php

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "config.php";
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "functions.php";

$dataLogin = json_decode(file_get_contents('php://input'), true);

if (empty($dataLogin["email"]) || empty($dataLogin["password"])) {
    echo curlResponse("OneOfTheFieldsIsEmpty");
}

if (!empty($dataLogin["email"]) && !empty($dataLogin["password"])) {
    $file = json_decode(
        file_get_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "users.json"),
        true
    );
    $user = array_filter($file, function ($user) use ($dataLogin) {
        if ($dataLogin["email"] === $user["email"] && $dataLogin["password"] === $user["password"]) {
            return $user;
        }
    });
    if (!empty($user)) {
        echo curlResponse("SuccessfullyAuthorized");
    }
    if (empty($user)) {
        echo curlResponse("SomethingWentWrong");
    }
}
