<?php

require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "config.php";
require_once dirname(__FILE__) . DIRECTORY_SEPARATOR . "functions.php";

$dataRegistration = json_decode(file_get_contents('php://input'), true);

if (!empty($dataRegistration)) {
    if ($dataRegistration["password"] !== $dataRegistration["confirmedPassword"]) {
        echo curlResponse("PasswordsDoNotMatch");
    }
    if ($dataRegistration["password"] === $dataRegistration["confirmedPassword"]) {
        unset($dataRegistration["confirmedPassword"]);
        $file = json_decode(
            file_get_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "users.json"),
            true
        );
        $postData[] = $dataRegistration;
        $userLogin = array_search($postData[0]["login"], array_column($file, "login"));
        $userEmail = array_search($postData[0]["email"], array_column($file, "email"));
        if (is_numeric($userLogin)) {
            echo curlResponse("ThisUsernameAlreadyExists");
        }
        if (is_numeric($userEmail)) {
            echo curlResponse("ThisEmailIsAlreadyTaken");
        }

        if ($userLogin === false && $userEmail === false) {
            $file = array_merge($file, $postData);
            $postDataJson = json_encode($file);
            file_put_contents(ROOT_PATH . DIRECTORY_SEPARATOR . "users.json", $postDataJson);
            echo curlResponse("RegistrationSuccessful");
        }
    }
}
