<?php

function curlResponse(string $data): string
{
    header('Content-Type: application/json; charset=utf-8');
    return json_encode($data);
}
